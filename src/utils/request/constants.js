export const GET = 'get';
export const POST = 'post';
export const UPDATE = 'update';
export const DELETE = 'delete';
export const BASE_URL = 'http://rest.velocorner.com';