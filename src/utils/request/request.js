import axios from 'axios';
import {
  GET,
  BASE_URL
} from './constants';

const request = ({
  data = {},
  method = GET,
  url = '',
}) => axios({
  data,
  method,
  url: [BASE_URL, url].join('/'),
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
  },
});

export default request;