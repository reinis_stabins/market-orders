import React, { PureComponent } from 'react';

const prefetch = (actions = []) => (Component) => (
  class Prefetcher extends PureComponent {
    componentWillMount() {
      if (actions.length) {
        actions.forEach(action => {
          action();
        });
      }
    }
    render() {
      return <Component {...this.props} />;
    }
  }
);

export default prefetch;