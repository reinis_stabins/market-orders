import styledNormalize from 'styled-normalize';
import { injectGlobal } from 'styled-components';
import theme from './theme';

export default () => injectGlobal`
  ${styledNormalize}

  html, body {
    font-family: 'Raleway', sans-serif;
    background-color: ${theme.colors.white};
    width: 100%;
    height: 100%;
    min-height: 100%;
    margin: 0;
    padding: 0;
    letter-spacing: -1px;
    line-height: 1.02;
  }

  a {
    text-decoration: none;
  }
  h1,h2,h3,h4,h5,p,a {
    margin: 0;
    padding: 0;
  }
`;