export default {
  white: '#fff',
  black: '#080707',
  grey: '#656565',
  lightGrey: '#a2a2a2',
  tableLightGrey: '#efefef',
  darkGrey: '#3c3c3c',
  lightImageBorder: '#0000001a',
  deleteButton: '#f14343',
  defaultButton: '#38afec',
  rowOverlay: '#e6e6e670',
};