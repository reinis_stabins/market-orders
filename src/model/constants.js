export const SUPPORTED_CURRENCY_PAIRS = 'supportedCurrencyPairs';

export const RATE_SNAPSHOT = 'rateSnapshot';

export const CREATE_ORDER = 'createOrder';

export const CANCEL_ORDER = 'cancelOrder';

export const RETRIEVE_ORDERS = 'retrieveOrders';