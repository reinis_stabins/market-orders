import request, {
  POST,
} from '../utils/request';
import {
  SUPPORTED_CURRENCY_PAIRS,
  RATE_SNAPSHOT,
  CREATE_ORDER,
  CANCEL_ORDER,
  RETRIEVE_ORDERS,
} from './constants';

export const getSupportedCurrencyPairs = () => request({
  url: SUPPORTED_CURRENCY_PAIRS,
});

export const getRateSnapshot = () => request({
  url: RATE_SNAPSHOT,
});

export const createOrder = data => request({
  url: CREATE_ORDER,
  method: POST,
  data,
});

export const cancelOrder = id => request({
  url: CANCEL_ORDER,
  method: POST,
  data: { id },
});

export const getOrders = () => request({
  url: RETRIEVE_ORDERS,
});