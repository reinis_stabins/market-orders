import styled from 'styled-components';

export const StyledSpacer = styled.div`
  margin: 40px 30px;
  max-width: 1000px;
  width: 100%;
`;