import React from 'react';
import PropTypes from 'prop-types';
import { StyledSpacer } from './spacerStyles';

const Spacer = ({ children }) => (
  <StyledSpacer>{children}</StyledSpacer>
);

Spacer.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
};

export default Spacer;