import styled from 'styled-components';
import { DELETE } from './constants';

export const StyledButton = styled.button`
  ${({ theme: { colors }, type, disabled }) => `
    background-color: ${type === DELETE ? colors.deleteButton : colors.defaultButton};
    color: ${colors.white};
    opacity: ${disabled ? '.7' : 1};
    pointer-evenets: ${disabled ? 'none' : 'initial'};
    cursor: ${disabled ? 'default' : 'pointer'};
  `}
  border: none;
  margin: 0;
  padding: 6px 10px;

  svg {
    fill: ${({ theme }) => theme.colors.black};
    width: 20px;
  }
`;

export const StyledInnerWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;