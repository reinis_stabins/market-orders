import React from 'react';
import PropTypes from 'prop-types';
import { StyledButton, StyledInnerWrapper } from './buttonStyles';
import { SUBMIT, DELETE } from './constants';

const Button = ({
  type = SUBMIT,
  children,
  ...rest,
}) => (
  <StyledButton {...rest} type={type}>
    <StyledInnerWrapper>
      {children}
    </StyledInnerWrapper>
  </StyledButton>
);

Button.propTypes = {
  type: PropTypes.oneOf([SUBMIT, DELETE]),
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
    PropTypes.string,
  ]),
};

export default Button;