import styled from 'styled-components';

const rowStyle = styled.div`
  padding: 6px;
  border-bottom: 1px solid ${({ theme }) => theme.colors.lightGrey};
  display: flex;
  align-items: center;
`;

const commonFlexboxStyles = `
  display: flex;
  flex-direction: column;
`;

export const StyledTable = styled.div`
  ${commonFlexboxStyles}
  position: relative;
`;

export const StyledHeaders = styled(rowStyle)`
  font-weight: 600;
`;

export const StyledHeader = styled.div`
  width: ${({ width }) => `${width}%`};
`;

export const StyledTableBody = styled.div`
  ${commonFlexboxStyles}
`;

export const StyledTableRow = styled(rowStyle)`
  position: relative;
  &:nth-child(2n) {
    background: ${({ theme }) => theme.colors.tableLightGrey};
  }
  &:last-child {
    border: none;
  }
`;

export const StyledTableCell = styled.div`
  width: ${({ width }) => `${width}%`};
`;

export const StyledButtonWrapper = styled.div`
  display: flex;
  width: 5%;
  justify-content: flex-end;
`;

export const StyledEmptyMessage = styled.div`
  width: 100%;
  text-align: center;
  padding: 20px 0;
`;