import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import store from '../../store';
import {
  StyledTable,
  StyledHeaders,
  StyledHeader,
  StyledTableBody,
  StyledEmptyMessage,
} from './tableStyles';
import TableRow from './TableRow';
import { FULL_WIDTH, EMPTY, HEADERS } from './constants';

const Table = ({
  headers = HEADERS,
  data = [],
}) => (
  <StyledTable>
    <StyledHeaders>
      {headers.map((header, index) => (
        <StyledHeader key={index} width={FULL_WIDTH / headers.length}>
          {header}
        </StyledHeader>
      ))}
    </StyledHeaders>
    {(!data.length && store.getIsAnyOrderDeleted) && <StyledEmptyMessage>{EMPTY}</StyledEmptyMessage>}
    <StyledTableBody>
      {data.map((row = {}, index) => (
        <TableRow data={row} key={index} />
      ))}
    </StyledTableBody>
  </StyledTable>
);

Table.propTypes = {
  headers: PropTypes.arrayOf(PropTypes.string),
  data: PropTypes.object,
};

export default observer(Table);