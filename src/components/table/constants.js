export const FULL_WIDTH = 95;
export const EMPTY = 'No orders are available!';
export const HEADERS = [
  'id',
  'Investment',
  'Should buy',
  'Counter',
  'Limit',
  'Valid date',
];