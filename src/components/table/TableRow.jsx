import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import store from '../../store';
import Button, { DELETE } from '../../components/button';
import TrashIcon from '../../../assets/trash.svg';
import {
  StyledTableRow,
  StyledTableCell,
  StyledButtonWrapper,
} from './tableStyles';
import { FULL_WIDTH } from './constants';

class TableRow extends PureComponent {
  constructor(args) {
    super(args);
    this.state = {
      isDeleting: false,
    };
  }

  deleteRow = id => () => {
    this.setState({ isDeleting: true }, () => {
      setTimeout(() => this.setState({ isDeleting: false }, () =>
        store.deleteOrder(id)
      ), 200);
    });
  }

  render() {
    const { data } = this.props;
    if (!data) return null;
    return (
      <StyledTableRow>
        {Object.keys(data).map((options, index) => (
          <StyledTableCell key={index} width={FULL_WIDTH / Object.keys(data).length}>
            {`${data[options]}`}
          </StyledTableCell>
        ))}
        <StyledButtonWrapper>
          <Button
            onClick={this.deleteRow(data.id)}
            type={DELETE}
            disabled={this.state.isDeleting}
          >
            <TrashIcon />
          </Button>
        </StyledButtonWrapper>
      </StyledTableRow>
    );
  }
};

TableRow.propTypes = {
  data: PropTypes.object,
};

export default TableRow;