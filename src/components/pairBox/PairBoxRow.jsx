import React from 'react';
import PropTypes from 'prop-types';
import {
  StyledPairBoxRow,
  StyledRateWrapper,
  StyledCurrencyWrapper,
} from './pairBoxStyles';
import {
  BID_TEXT,
  ASK_TEXT,
  PAIR_TEXT,
} from './constants';

const PairBoxRow = ({
  rate = {},
  currency = {},
}) => (
  <StyledPairBoxRow>
    <StyledRateWrapper>
      <div><span>{BID_TEXT}</span>{rate.bid || '-'}</div>
      <div><span>{ASK_TEXT}</span>{rate.ask || '-'}</div>
    </StyledRateWrapper>
    <StyledCurrencyWrapper>
      <label>{PAIR_TEXT}</label>
      <div>{currency.ccy1} - {currency.ccy2}</div>
    </StyledCurrencyWrapper>
  </StyledPairBoxRow>
);

PairBoxRow.propTypes = {
  rate: PropTypes.shape({
    bid: PropTypes.number,
    ask: PropTypes.number,
  }),
  currency: PropTypes.shape({
    ccy1: PropTypes.string,
    ccy2: PropTypes.string,
  }),
};

export default PairBoxRow;