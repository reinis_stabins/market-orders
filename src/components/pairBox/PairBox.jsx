import React from 'react';
import PropTypes from 'prop-types';
import { StyledPairBox } from './pairBoxStyles';

const PairBox = ({
  children,
}) => (
  <StyledPairBox>
    {children}
  </StyledPairBox>
);

PairBox.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
};

export default PairBox;