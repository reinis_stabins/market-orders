import styled from 'styled-components';

export const StyledPairBox = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
`;

export const StyledPairBoxRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 6px;
  border-bottom: 1px solid ${({ theme }) => theme.colors.lightGrey};
  &:nth-child(2n) {
    background: ${({ theme }) => theme.colors.tableLightGrey};
  }
  &:last-child {
    border-bottom: none;
  }
`;

export const StyledRateWrapper = styled.div`
  span {
    margin-right: 10px;
    font-weight: 600;
  }
`;

export const StyledCurrencyWrapper = styled.div`
  text-align: right;
  label {
    font-weight: 600;
  }
`;