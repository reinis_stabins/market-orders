import React from 'react';
import PropTypes from 'prop-types';
import { StyledTitle } from './titleStyles';

const Title = ({ children }) => (
  <StyledTitle>
    {children}
  </StyledTitle>
);

Title.propTypes = {
  children: PropTypes.string,
};

export default Title;