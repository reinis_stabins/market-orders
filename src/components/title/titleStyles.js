import styled from 'styled-components';

export const StyledTitle = styled.h3`
  font-size: 26px;
  color: {({ theme }) => theme.color.black};
  margin: 0 0 10px;
`;