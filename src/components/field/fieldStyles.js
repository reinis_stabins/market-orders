import styled from 'styled-components';

export const StyledField = styled.input`
  border: none;
  padding: 0;
  margin: 0;
  width: 100%;
  height: 45px;
  border-bottom: 1px solid ${({ theme }) => theme.colors.lightGrey};
  &:focus {
    outline: none;
  }
`;

export const StyledSelect = styled.div`
  border: none;
  padding: 0;
  margin: 0;
  width: 100%;
  height: 45px;
  position: relative;
  &:focus {
    outline: none;
  }
  border: 1px solid ${({ theme }) => theme.colors.lightGrey};
  display: flex;
  align-items: center;
  select {
    position: absolute;
    top: 0; left: 0; right: 0; bottom: 0;
    opacity: 0;
    width: 100%;
    height: 100%;
    cursor: pointer;
  }
`;

export const StyledFieldWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin: 0 0 20px;
`;

export const StyledLabel = styled.div`
  font-weight: 600;
  margin-bottom: 5px;
`;

export const StyledInnerWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0 10px;
  width: 100%;
  align-items: center;
  svg {
    width: 25px;
    height: 25px;
  }
`;