import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import store from '../../store';
import {
  StyledFieldWrapper,
  StyledField,
  StyledLabel,
} from './fieldStyles';
import {
  TYPE_TEXT,
  DEFAULT_PLACEHOLDER,
} from './constants';

const onChange = name => ({ target: { value } }) => {
  store.changeFieldValue({
    name,
    value,
  });
};

const Field = ({
  type = TYPE_TEXT,
  label,
  name,
  ...rest,
}) => (
  <StyledFieldWrapper>
    {label && <StyledLabel>{label}</StyledLabel>}
    <StyledField
      {...rest}
      onChange={onChange(name)}
      value={store[name]}
      placeholder={DEFAULT_PLACEHOLDER}
      type={type}
    />
  </StyledFieldWrapper>
);

Field.propTypes = {
  label: PropTypes.string,
  type: PropTypes.oneOf([TYPE_TEXT]),
  name: PropTypes.string.isRequired,
};

export default observer(Field);