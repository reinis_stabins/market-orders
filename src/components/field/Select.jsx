import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import store from '../../store';
import {
  StyledFieldWrapper,
  StyledSelect,
  StyledLabel,
  StyledInnerWrapper,
} from './fieldStyles';
import SelectIcon from '../../../assets/select.svg';

const onChange = name => ({ target: { value } }) => {
  store.changeFieldValue({
    name,
    value,
  });
};

const Select = ({
  label,
  options = [],
  name,
  ...rest,
}) => (
  <StyledFieldWrapper>
    {label && <StyledLabel>{label}</StyledLabel>}
    <StyledSelect>
      <StyledInnerWrapper>
        <span>{`${store[name]}`}</span>
        <SelectIcon />
      </StyledInnerWrapper>
      <select
        onChange={onChange(name)}
      >
        {options.map(({value, text}, index) =>
          <option value={value} key={index}>
            {text}
          </option>
        )}
      </select>
    </StyledSelect>
  </StyledFieldWrapper>
);

Select.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.any,
    text: PropTypes.string,
  })),
};

export default observer(Select);