import styled from 'styled-components';

export const StyledBoxWrapper = styled.div`
  border: 1px solid ${({ theme }) => theme.colors.lightGrey};
`;