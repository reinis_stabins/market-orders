import React from 'react';
import PropTypes from 'prop-types';
import { StyledBoxWrapper } from './boxWrapperStyles';

const BoxWrapper = ({ children }) => (
  <StyledBoxWrapper>
    {children}
  </StyledBoxWrapper>
);

BoxWrapper.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
};

export default BoxWrapper;