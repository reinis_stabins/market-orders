import { observable, action, computed } from 'mobx';
import isEqual from 'lodash.isequal';
import remove from 'lodash.remove';
import {
  getSupportedCurrencyPairs,
  getOrders,
  getRateSnapshot,
  createOrder,
  // cancelOrder,
} from '../model';

class Store {

  @observable currencyPairs = observable([]);
  @observable orders = observable([]);
  @observable rateSnapshot = observable([]);
  @observable isAnyOrderDeleted = false;

  @observable investmentCurrencyField = '';
  @observable buyField = true;
  @observable counterCurrencyField = '';
  @observable limitField = '';
  @observable validUntilField = '';
  @observable orderFormPending = false;

  @action fetchCurrencyPairs = async () => {
    const { data } = await getSupportedCurrencyPairs();
    this.currencyPairs = observable(data);
  };

  @action fetchOrders = async () => {
    const { data } = await getOrders();
    this.orders = observable(data);
  };

  @action fetchRateSnapshot = async () => {
    const { data } = await getRateSnapshot();
    this.rateSnapshot = observable(data);
  };

  @action deleteOrder = async id => {
    // TODO: should uncomment this line when endpoint will work
    // const { data } = await cancelOrder(id);

    this.orders = this.orders.filter(order => order && order.id !== id);
    this.isAnyOrderDeleted = true;
  };

  @action createOrder = async () => {
    this.orderFormPending = true;
    const data = {
      investmentCcy: this.investmentCurrencyField,
      buy: this.buyField,
      counterCcy: this.counterCurrencyField,
      limit: this.limitField,
      validUntil: this.validUntilField,
    };
    const result = await createOrder(data);
    this.orders.unshift(result.data);
    this.orderFormPending = false;
    this.resetFields();
  };

  @action resetFields = () => {
    this.investmentCurrencyField = '';
    this.buyField = true;
    this.counterCurrencyField = '';
    this.limitField = '';
    this.validUntilField = '';
    this.orderFormPending = false;
  };

  @action changeFieldValue = ({ name = '', value = '' }) => {
    this[name] = value;
  };

  @computed get getCurrencyPairs() {
    return this.currencyPairs;
  }

  @computed get getOrders() {
    return this.orders;
  }

  @computed get getRateSnapshot() {
    return this.rateSnapshot;
  }

  @computed get getSupportedPairs() {
    return this.rateSnapshot.filter(({ ccyPair }) => (
      this.currencyPairs.some((pair) => isEqual(pair, ccyPair)))
    );
  }

  @computed get getIsAnyOrderDeleted() {
    return this.isAnyOrderDeleted;
  }

}

export default new Store();