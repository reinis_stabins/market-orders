import React from 'react';
import { hot } from 'react-hot-loader';
import { ThemeProvider } from 'styled-components';
import Views from 'views';
import { globalStyles, theme } from './utils/styles';

globalStyles();

const App = () => (
  <ThemeProvider theme={theme}>
    <Views />
  </ThemeProvider>
);

export default hot(module)(App);