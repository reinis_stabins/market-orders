import React, { Fragment } from 'react';
import { observer } from 'mobx-react';
import prefetch from '../../utils/prefetch';
import store from '../../store';
import Table from '../../components/table';
import BoxWrapper from '../../components/boxWrapper';
import Title from '../../components/title';
import { TITLE } from './constants';

const OrderOverview = () => {
  return (
    <Fragment>
      <Title>
        {TITLE}
      </Title>
      <BoxWrapper>
        <Table data={store.orders} />
      </BoxWrapper>
    </Fragment>
  );
};

export default prefetch([
  store.fetchOrders,
])(observer(OrderOverview));