import React from 'react';
import { observer } from 'mobx-react';
import store from '../../store';
import Field, { Select } from '../../components/field';
import Title from '../../components/title';
import Button from '../../components/button';
import { StyledForm } from './orderCreatorStyles';
import {
  TITLE,
  INVESTMENT_CURRENCY_LABEL,
  BUY_OPTIONS,
  COUNTER_CURRENCY_LABEL,
  LIMIT_LABEL,
  VALID_UNTIL_LABEL,
  BUTTON_TEXT,
} from './constants';

const MarketOverview = () => (
  <StyledForm>
    <Title>{TITLE}</Title>
    <Field
      name="investmentCurrencyField"
      label={INVESTMENT_CURRENCY_LABEL}
    />
    <Select
      name="buyField"
      options={BUY_OPTIONS}
      label={INVESTMENT_CURRENCY_LABEL}
    />
    <Field
      name="counterCurrencyField"
      label={COUNTER_CURRENCY_LABEL}
    />
    <Field
      name="limitField"
      label={LIMIT_LABEL}
    />
    <Field
      name="validUntilField"
      label={VALID_UNTIL_LABEL}
    />
    <Button
      disabled={store.orderFormPending}
      onClick={store.createOrder}
    >
      {BUTTON_TEXT}
    </Button>
  </StyledForm>
);

export default observer(MarketOverview);