export const TITLE = 'Add new order';
export const INVESTMENT_CURRENCY_LABEL = 'Investment currency';
export const COUNTER_CURRENCY_LABEL = 'Counter currency';
export const LIMIT_LABEL = 'Limit';
export const VALID_UNTIL_LABEL = 'Valid until';
export const BUTTON_TEXT = 'Create order';
export const BUY_OPTIONS = [
  {
    value: true,
    text: 'true',
  },
  {
    value: false,
    text: 'false',
  },
];