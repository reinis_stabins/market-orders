import styled from 'styled-components';

export const StyledForm = styled.div`
  button {
    width: 100%;
    padding: 15px 10px;
  }
`;