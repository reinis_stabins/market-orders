import React from 'react';
import MarketOverview from '../views/marketOverview';
import OrdersOverview from '../views/ordersOverview';
import OrderCreator from '../views/orderCreator';
import Spacer from '../components/spacer';
import {
  StyledViewsWrapper,
  StyledViewsLeftSide,
  StyledViewsRightSide,
  StyledViewsContainer,
  StyledViewsRoot,
} from './viewsStyles';

const Views = () => (
  <StyledViewsRoot>
    <Spacer>
      <StyledViewsWrapper>
        <StyledViewsLeftSide>
          <StyledViewsContainer>
            <OrderCreator />
          </StyledViewsContainer>
        </StyledViewsLeftSide>
        <StyledViewsRightSide>
          <StyledViewsContainer>
            <OrdersOverview />
          </StyledViewsContainer>
          <StyledViewsContainer>
            <MarketOverview />
          </StyledViewsContainer>
        </StyledViewsRightSide>
      </StyledViewsWrapper>
    </Spacer>
  </StyledViewsRoot>
);

export default Views;