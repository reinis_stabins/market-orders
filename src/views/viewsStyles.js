import styled from 'styled-components';

export const StyledViewsRoot = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  justify-content: center;
`;

export const StyledViewsWrapper = styled.div`
  display: flex;
`;

export const StyledViewsLeftSide = styled.div`
  width: 40%;
  display: flex;
  flex-direction: column;
  padding: 0 80px 0 0;
`;

export const StyledViewsRightSide = styled.div`
  width: 60%;
`;

export const StyledViewsContainer = styled.div`
  margin-bottom: 40px;
`;