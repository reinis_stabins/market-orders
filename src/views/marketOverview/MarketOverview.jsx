import React, { Fragment } from 'react';
import { observer } from 'mobx-react';
import prefetch from '../../utils/prefetch';
import store from 'store';
import PairBox, { PairBoxRow } from '../../components/pairBox';
import BoxWrapper from '../../components/boxWrapper';
import Title from '../../components/title';
import { TITLE } from './constants';

const MarketOverview = () => {
  return (
    <Fragment>
      <Title>
        {TITLE}
      </Title>
      <BoxWrapper>
        <PairBox>
          {(store.getSupportedPairs.length ? store.getSupportedPairs : Array(8).fill({}))
            .map(({ ccyPair, ...rest }, index) => (
              <PairBoxRow
                key={index}
                currency={ccyPair}
                rate={rest}
              />
            ))}
        </PairBox>
      </BoxWrapper>
    </Fragment>
  );
};

export default prefetch([
  store.fetchCurrencyPairs,
  store.fetchRateSnapshot,
])(observer(MarketOverview));