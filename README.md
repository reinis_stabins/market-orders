# Market orders exercise

###See live [Demo](http://order-overview.surge.sh/)

#### Requirements for running project

1. Node version >= 8.9.4
1. Yarn installed on your machine

### How to run project

Install all dependencies
```
$ yarn
```

Run development build
```
$ yarn start
```

### Project developed on OSX

### TODO

1. Adjust css for responsive design
1. Create order form validation
1. Cancel order api request fix
1. Cover Domain model logic with unit tests
1. Faild request error handling

